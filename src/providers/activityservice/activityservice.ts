import { Injectable } from '@angular/core';
import * as firebase from "firebase";
import 'firebase/firestore';

@Injectable()
export class ActivityService {
    db : any;
    listActivity: any;
    emptyschedule: any;

  constructor() {
    this.db = firebase.firestore();
  }

  create(local: any, schedule: any, type : String, user: String){
    let promise = new Promise((resolve, reject) => {

        // Add a new document in collection "cities"
        console.log(local);
        var scheduleact = schedule.allschedule[schedule.id];
        scheduleact.date = new Date(scheduleact.date);
        var activityConfig = {
            full: false,
            local: local.name,
            address: local.address,
            image: local.image,
            size: 4,
            type: type,
            schedule: scheduleact,
            user: [user]
        };

        //Create activity document
        this.db.collection("activity").add(activityConfig)
        .then( response => {
            console.log("Document successfully written!");
            //Add activity id to User
            this.addActivityToUser(user, response.id).then(response=>{
                
                //Refresh schedule of the local
                let newSchedule = schedule.allschedule;
                newSchedule[schedule.id].empty = false;
                this.db.collection("local").doc(local.name)
                .update({schedule: newSchedule}).then(response=>{
                    resolve(activityConfig);
                    this.getAllActivity();
                    this.getEmptyActivity();
                }).catch(err=>{
                    reject(err);
                });

            }).catch(err =>{
                reject(err);
            })
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            reject(error);
        });
    });
    return promise;
  }

  deleteActivity(activityData: any){
    let promise = new Promise((resolve, reject) => {

        this.db.collection("activity").doc(activityData.id).delete().then(response => {
            console.log("Document successfully deleted!");
            this.db.collection("local").doc(activityData.local).get().then(response =>{
                let newschedule = response.data().schedule;
                newschedule.forEach(function(element) {
                    if(activityData.schedule.date.toString == element.date.toString){
                        element.empty = true;
                    }
                });
                this.db.collection("local").doc(activityData.local).update({schedule: newschedule}).then(res =>{
                    resolve(newschedule);
                    this.getAllActivity();
                    this.getEmptyActivity();
                })
            }).catch(err =>{
                console.log(err);
            })
        }).catch(function(error) {
            console.error("Error removing document: ", error);
            reject(error);
        });

    });

    return promise;
  }

  modifyUserToActivity(users: any, activityID: String){
    let promise = new Promise((resolve, reject) => {
        
            this.db.collection("activity").doc(activityID)
            .update({user: users}).then(response=>{
                resolve(users); 
            }).catch(err=>{
                reject(err);
            });
          });
    return promise;
  }

  addActivityToUser(user: any, activityID: String){
    let promise = new Promise((resolve, reject) => {
        
            this.db.collection("user").doc(user).get().then(response=>{
                let listAct = response.data().activity;
                if(listAct == null){
                    listAct = [activityID];
                }else{
                    listAct.push(activityID);
                }
                this.db.collection("user").doc(user)
                .update({activity: listAct}).then(response=>{
                    resolve(); 
                }).catch(err=>{
                    reject(err);
                });

            }).catch(err=>{
                reject(err);
            });
          });
    return promise;
  }

  removeActivityToUser(user: any, activityID: String){
    let promise = new Promise((resolve, reject) => {
        
            this.db.collection("user").doc(user).get().then(response=>{
                let listAct = response.data().activity;
                    listAct.pop(activityID);
                this.db.collection("user").doc(user)
                .update({activity: listAct}).then(response=>{
                    resolve(); 
                }).catch(err=>{
                    reject(err);
                });

            }).catch(err=>{
                reject(err);
            });
          });
    return promise;
  }

  getEmptyActivity(){
    let promise = new Promise((resolve, reject) => {

        this.db.collection("local").get().then(response=>{
            let localList: any = [];
            response.forEach(doc => {
                console.log(doc.id, '=>', doc.data());
                localList.push(doc.data());
            });
            let emptySchedule: any[] = [];

            //Parse all local
            localList.forEach(elementLocal => {
                for (let i in elementLocal.schedule) {
                    console.log(elementLocal.schedule[i]);

                    //If schedule of a local is empty add tom emptyActivitySchedule
                    if(elementLocal.schedule[i].empty){
                        let scheduletemp = elementLocal.schedule[i];
                        scheduletemp.id = i;
                        scheduletemp.allschedule = JSON.parse(JSON.stringify(elementLocal.schedule));
                        let scheduleItem = {
                            schedule : scheduletemp,
                            local: elementLocal
                        };
                        emptySchedule.push(scheduleItem);
                    }
                }
            });
        resolve(emptySchedule); 
        this.setEmptyActivity(emptySchedule);
    }).catch(err=>{
        reject(err);
    });
  });
    return promise;
  }

  getAllActivity(){
    let promise = new Promise((resolve, reject) => {

        this.db.collection('activity').get()
        .then(snapshot => {
            let localList: any[] = [];
            snapshot.forEach(doc => {
                console.log(doc.id, '=>', doc.data());
                var tempact = doc.data();
                tempact.id = doc.id;
                localList.push(tempact);
            });
            resolve(localList);
            this.setListActivity(localList);
        })
        .catch(err => {
            console.log('Error getting documents', err);
            reject(err);
        });

    });
    return promise;
  }

  formatDate(unformat_date : Date){
    let days = ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"];
    let months = ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", 
     "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"];
    let date = new Date(unformat_date);
    let f_date = days[date.getDay()];  
    f_date += " " + date.getDate() + ", " + months[date.getMonth()];
    let minutes = "";
    (date.getMinutes() <10) ? minutes = "0" + date.getMinutes() : minutes = "" + date.getMinutes(); 
    return date.getHours() + ":" + minutes + " " +  f_date;
   }

   setListActivity(listact){
       this.listActivity = listact;
   }

   getListActivity(){
       return this.listActivity;
   }

   setEmptyActivity(emptact){
       this.emptyschedule = emptact;
   }

   getEmptyActivityf(){
       return this.emptyschedule;
   }
}