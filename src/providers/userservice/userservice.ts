import { Injectable } from '@angular/core';
import * as firebase from "firebase";
import 'firebase/firestore';

@Injectable()
export class UserService {
    
    db: any;
    public activityList: any;

  constructor() {
    this.db = firebase.firestore();
  }

  getUser(email: String){

    let promise = new Promise((resolve, reject) => {
        
                this.db.collection('user').doc(email).get()
                .then(doc => {
                    if (!doc.exists) {
                        console.log('No such document!');
                        reject('No such document!');
                    } else {
                        console.log('Document data:', doc.data());
                        resolve(doc.data());
                    }
                })
                .catch(err => {
                    console.log('Error getting document', err);
                    reject(err);
                });
    });
            
    return promise;
  }

  createUser(email: String){
    let promise = new Promise((resolve, reject) => {
        
                this.db.collection('user').doc(email).set({
                    nick: email,
                    description: null
                })
                .then(response => {
                        resolve(response);
                })
                .catch(err => {
                    reject(err);
                });
    });
            
    return promise;
  }

  getUserActivity(user: any){
    let promise = new Promise((resolve, reject) => {
        let activityList = [];    
        if(user.activity != null){
            
            this.db.collection("activity").get()
            .then(res => {
                res.forEach(doc => {
                    console.log(doc.id, " => ", doc.data());
                    if(user.activity.includes(doc.id)){
                        let activity = doc.data();
                        activity.id = doc.id;
                        activityList.push(activity);
                    }
                });
                resolve(activityList);
                this.setActivityList(activityList);
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
                reject(error);
            });
        }else{
            resolve(activityList);
            this.setActivityList(activityList);
        }
    });
            
    return promise;
  }

  getActivityList(){
      return this.activityList;
  }

  setActivityList(actList: any){
    this.activityList = actList;
  }

  addActivityToList(activity: any){
    this.activityList.push(activity);
  }

  removeActivityToList(activity: any){
    this.activityList.pop(activity);
  }

  

}