import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class Uiproviders {

    public loader: any;
    public toast: any;
    public loader_present: boolean;
    public loader_starter: boolean;
    public cont : string = "Aspetta";

  constructor(public loadingCtrl: LoadingController) {
    this.loader = this.loadingCtrl.create({
        content: this.cont,
      });

  }

  loaderSetContent(cont: string){
    this.loader.setContent(cont);
    this.cont = cont;
  }

  loaderPresent(){
    this.loader_starter = true;
    this.loader.present().then(()=>{
      if(this.loader_starter){
        this.loader_present = true;
      }else{
        this.loader.dismiss();
        this.loader = this.loadingCtrl.create({
          content: this.cont,
        });
        this.loader_present = false;
      }
    });
    
    console.log("Loader SHOW, loaderstatus: " + this.loader_present);
  }

  loaderDismiss(){
    console.log("Loader HIDE, loaderstatus: " + this.loader_present);
    if(this.loader_present){
      try{
        this.loader.dismiss();
        this.loader = this.loadingCtrl.create({
          content: this.cont,
        });
      }catch(err){
        console.log(err);
      }
        this.loader_present = false;
    }
    this.loader_starter = false;
  }

  

}