import { Injectable } from '@angular/core';
import * as firebase from "firebase";
import 'firebase/firestore';

@Injectable()
export class Local {
    db: any;
  constructor() {
    this.db = firebase.firestore();
  }

  add(loc: any){
    // Add a new document in collection "cities"
    this.db.collection("local").doc(loc.name).set({
        name: loc.name,
        address: loc.address,
        image: loc.image,
        schedule: []
    })
    .then(function() {
        console.log("Document successfully written!");
    })
    .catch(function(error) {
        console.error("Error writing document: ", error);
    });
  }

  getAllLocal(){

    let promise = new Promise((resolve, reject) => {

        this.db.collection('local').get()
        .then(snapshot => {
            let localList: any[] = [];
            snapshot.forEach(doc => {
                console.log(doc.id, '=>', doc.data());
                localList.push(doc.data());
            });
            resolve(localList);
        })
        .catch(err => {
            console.log('Error getting documents', err);
            reject(err);
        });
    });
    
    return promise;
  }

  updateSchedule(local: any, schedule: any){

  /*  let localList: any[] = [];
    for (let i in local.schedule) {
        this.schedulesToModify.push(local.schedule[i]);
    }*/

    let promise = new Promise((resolve, reject) => {
        
                this.db.collection("local").doc(local.name).update({
                   "schedule": schedule
                })
                .then(function() {
                    console.log('Schedule updated!');
                    resolve();
                }).catch(err => {
                    console.log('Error update schedule', err);
                    reject(err);
                });
            });
            
            return promise;
  }

}