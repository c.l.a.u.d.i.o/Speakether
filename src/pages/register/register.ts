import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from "firebase";
import { User } from '../../models/user';
import { UserService } from "../../providers/providers";
import { Uiproviders } from "../../providers/providers";
import { UserPage } from "../user/user";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = {} as User;
  error_message: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userSrv: UserService,
      public ui: Uiproviders) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  async register(user: User){

    try{
     //Register the user on firebase
     this.ui.loaderPresent();
     const result = await firebase.auth().createUserWithEmailAndPassword(user.email, user.password);
     if(result){
        //Register the user in firestore DB
        this.userSrv.createUser(user.email).then(response =>{
          console.log("User Created");

          //Login the user
          firebase.auth().signInWithEmailAndPassword(user.email, user.password).then(response =>{
            console.log("Logged in");
            this.navCtrl.push(UserPage);
          })

        }).catch(err =>{
          console.log(err);
          this.error_message = err.message;
          this.ui.loaderDismiss();
        })
     }
     console.log(result);
    }catch(e){
      console.log(e);
      this.error_message = e.message;
      this.ui.loaderDismiss();
    }
    }

}
