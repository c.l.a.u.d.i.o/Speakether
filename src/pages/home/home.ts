import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { ActivityService } from "../../providers/providers";
import { AddActivityPage } from "../add-activity/add-activity";
import * as firebase from "firebase";
import { ShowActivityPage } from '../show-activity/show-activity';
import { UserPage } from '../user/user';
import { Uiproviders } from "../../providers/providers";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public activity:ActivityService, public loadingCtrl: LoadingController,
    private toastCtrl: ToastController, public ui: Uiproviders) {
    this.ui.loaderSetContent("Un attimo di pazienza...");
    this.activity.getAllActivity().then(response =>{
      console.log( response);
      this.ui.loaderDismiss();
    }).catch(err=>{
      console.log(err);
      this.ui.loaderDismiss();
    })
  }

  addActivity(){
    console.log("Open ActivityPage");

    this.navCtrl.push(AddActivityPage);
    this.ui.loaderPresent();;

  }

  ionViewWillLoad(){
    console.log("Page Home Loaded");
  }

  showActivity(item: any){
    var user = firebase.auth().currentUser;
    if (user) {
      console.log(item);
      this.navCtrl.push(ShowActivityPage, {
        activityData: item,
        user: user.email
      })
    } else {
      console.log("Not user logged");
      this.navCtrl.push(UserPage);
    }

  }

}
