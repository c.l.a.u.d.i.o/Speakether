import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user';
import * as firebase from "firebase";
import { HomePage } from '../home/home';
import { ShowActivityPage } from '../show-activity/show-activity';

import { UserService, ActivityService } from "../../providers/providers";
import { Uiproviders } from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  loginuser = {} as User;
  curUser: any;
  error_message : String;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userSrv: UserService,
     public activity: ActivityService, public ui: Uiproviders) {
    
    let firebaseCrntUsr = firebase.auth().currentUser;
    if(firebaseCrntUsr){
      this.userSrv.getUser(firebase.auth().currentUser.email).then(response =>{
        this.curUser = response;
        console.log("User logged");

        console.log(response);
        this.loadActivity();
        this.ui.loaderDismiss();
      }).catch(err =>{
        console.log(err);
        this.ui.loaderDismiss();
      })
      console.log(this.curUser);
      }else{
        this.ui.loaderDismiss();
      }
    this.error_message = "Per poter continuare devi essere loggato";

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');

  }

  async login(loginuser: User){
    this.ui.loaderPresent();
    try{
      const result = await firebase.auth().signInWithEmailAndPassword(loginuser.email, loginuser.password);
      if(result){
        console.log("Logged!");
        this.curUser = await this.userSrv.getUser(firebase.auth().currentUser.email);
        console.log(this.curUser);
        this.loadActivity()
        this.ui.loaderDismiss();
      }
      
    }catch(e){
      this.ui.loaderDismiss();
      console.log(e);
    }
    
  }

  register(){
    this.navCtrl.push('RegisterPage');
  }

  signout(){
    this.ui.loaderPresent();
    firebase.auth().signOut().then(resposne =>{
      this.curUser = null;
      console.log("Signed out");
      this.userSrv.setActivityList([]);
      this.ui.loaderDismiss();
    }).catch(err =>{
      this.ui.loaderDismiss();
      console.log(err);
    })
  }

  loadActivity(){
    this.userSrv.getUserActivity(this.curUser).then(response=>{
            
          }).catch(err=>{
            console.log(err);
          })
  }


  showActivity(item: any){

      console.log(item);
      this.navCtrl.push(ShowActivityPage, {
        activityData: item,
        user: firebase.auth().currentUser.email
      })
  }

}
