import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActivityService, UserService } from "../../providers/providers";
import { Uiproviders } from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-show-activity',
  templateUrl: 'show-activity.html',
})
export class ShowActivityPage {
  activityData: any;
  user: any;
  userPresent: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actsrv: ActivityService, 
    public usrsrv: UserService, public ui: Uiproviders) {
    this.activityData = navParams.get('activityData');
    this.ui.loaderDismiss();
    this.user = navParams.get('user');
    if (this.user == null){
      this.userPresent = true;
    }else{
      this.userPresent = this.activityData.user.includes(this.user);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowActivityPage');
  }

  addUser(){
    this.ui.loaderSetContent("Aggiunta utente....");
    this.ui.loaderPresent();
    let users = this.activityData.user;
    users.push(this.user)
    this.actsrv.modifyUserToActivity(users, this.activityData.id).then(response =>{
      this.activityData.user = response;
      this.actsrv.addActivityToUser(this.user,this.activityData.id).then(response =>{
        this.userPresent = true;
        this.usrsrv.addActivityToList(this.activityData);
        this.ui.loaderDismiss();
        
      }).catch(err=>{
        console.log(err);
        this.ui.loaderDismiss();
      })

    }).catch(err =>{
      console.log(err);
      this.ui.loaderDismiss();
    })
  }

  removeUser(){
    this.ui.loaderSetContent("Disiscrizione utente....");
    this.ui.loaderPresent();
    let users = this.activityData.user;
    users.pop(this.user)
    this.actsrv.modifyUserToActivity(users, this.activityData.id).then(response =>{
      this.ui.loaderDismiss();
      if(users.length == 0){
        this.actsrv.deleteActivity(this.activityData).then(response =>{
          console.log("Activity deleted");
          
        }).catch(err =>{
          console.log(err);
          this.ui.loaderDismiss();
        })
      }
      this.activityData.user = response;
      this.actsrv.removeActivityToUser(this.user,this.activityData.id).then(response =>{
        this.userPresent = false;
        this.usrsrv.removeActivityToList(this.activityData);
        
      }).catch(err=>{
        console.log(err);
        this.ui.loaderDismiss();
      })

    }).catch(err =>{
      console.log(err);
      this.ui.loaderDismiss();
    })
  }

}
