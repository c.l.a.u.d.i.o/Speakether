import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowActivityPage } from './show-activity';

@NgModule({
  declarations: [
    ShowActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowActivityPage),
  ],
})
export class ShowActivityPageModule {}
