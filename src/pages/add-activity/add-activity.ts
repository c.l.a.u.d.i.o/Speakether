import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { ActivityService } from "../../providers/providers";
import { Local } from "../../providers/providers";
import { Uiproviders } from "../../providers/providers";

import { SelectActivityPage } from "../select-activity/select-activity";
import { UserPage } from "../user/user";

import * as firebase from "firebase";

@IonicPage()
@Component({
  selector: 'page-add-activity',
  templateUrl: 'add-activity.html',
})
export class AddActivityPage {
  private _schedule: any;

  constructor(public local: Local, public navCtrl: NavController, public navParams: NavParams,
    public activity:ActivityService, public ui: Uiproviders) {
    
    this.activity.getEmptyActivity().then(response =>{
      console.log("EmptyActivity" + response);
      this.ui.loaderDismiss();
    }).catch(err=>{
      console.log(err);
      this.ui.loaderDismiss();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddActivityPage');
  }

  newActivity(item : any){
    let firebaseCrntUsr = firebase.auth().currentUser;
    if(firebaseCrntUsr){
      console.log("Open SelectActivityPage");
      this.navCtrl.push(SelectActivityPage,  {
        local: item.local,
        schedule: item.schedule,
        user: firebaseCrntUsr.email
      });
    }
    else{
      this.navCtrl.push(UserPage);
      console.log("Not logged");
    }
  }

}
