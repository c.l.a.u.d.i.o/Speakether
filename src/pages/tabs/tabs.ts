import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';


import { HomePage } from '../home/home';
import { AddActivityPage } from '../add-activity/add-activity';
import { UserPage } from '../user/user';
import { AddLocalPage } from '../add-local/add-local';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AddActivityPage;
  tab3Root = UserPage;
  tab4Root = AddLocalPage;

  constructor(public navCtrl: NavController) {

  }

  popPage(){
    //this.navCtrl.pop();
  }
}
