import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddLocalPage } from './add-local';

@NgModule({
  declarations: [
    AddLocalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddLocalPage),
  ],
})
export class AddLocalPageModule {}
