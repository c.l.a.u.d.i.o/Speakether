import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Local } from "../../providers/providers";
import { ActivityService } from "../../providers/providers";


@IonicPage()
@Component({
  selector: 'page-add-local',
  templateUrl: 'add-local.html',
})
export class AddLocalPage {
  private localList: any;
  private localToAdd: any;
  private _local: Local;
  private schedulesToModify: Array<any> = [];
  private localToModify = '';
  private newscheduledate: any;
  private newscheduleduration = 60;

  constructor(public navCtrl: NavController, public activity: ActivityService, public navParams: NavParams, public local: Local) {
    local.getAllLocal().then(response =>{
      this.localList = response;
    }).catch(err => {});
    this._local = local;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddLocalPage');
  }

  ngOnInit() {
    this.localToAdd = {
      name: '',
      address: '',
      image: ""
    }
  }

  addLocal(){
    this._local.add(this.localToAdd);
  }

  showSchedule(local: any){
    this.schedulesToModify = [];
        for (let i in local.schedule) {
                this.schedulesToModify.push(local.schedule[i]);
        }
    console.log(this.schedulesToModify);
    this.localToModify = local;
  }

  saveSchedule(){
    let schedulesToModify_tmp = this.schedulesToModify;
    this.schedulesToModify.push({"date" : new Date(this.newscheduledate), "duration": this.newscheduleduration, "empty": true });
    this.local.updateSchedule(this.localToModify, this.schedulesToModify).then(response =>{
      
    }).catch(err => {});
      this.schedulesToModify = schedulesToModify_tmp;
  }
  

}
