import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectActivityPage } from './select-activity';

@NgModule({
  declarations: [
    SelectActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectActivityPage),
  ],
})
export class SelectActivityPageModule {}
