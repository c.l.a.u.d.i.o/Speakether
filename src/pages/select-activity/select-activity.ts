import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActivityService } from "../../providers/providers";
import { ShowActivityPage } from '../show-activity/show-activity';
import { UserService } from '../../providers/userservice/userservice';
import { Uiproviders } from "../../providers/providers";

@IonicPage()
@Component({
  selector: 'page-select-activity',
  templateUrl: 'select-activity.html',
})
export class SelectActivityPage {
  private local: any;
  private schedule: any;
  private user: any;
  private activityList: Array<any>;
  private choosen_activity : String = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public activirysrv: ActivityService,
  public usrsrv: UserService, public ui: Uiproviders) {
    this.local = navParams.get('local');
    this.schedule = navParams.get('schedule');
    this.user = navParams.get('user');
    this.activityList = [
      {
        name: 'Game',
        checked: false
      },
      {
        name: 'Lesson',
        checked: false
      },
      {
        name: 'Connect',
        checked: false
      }];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectActivityPage');
  }

  selectProduct(activity : String){
    console.log(this.activityList);
    for(var i = 0; i < this.activityList.length; i++){
      if(!(this.activityList[i].name == activity)){
        this.activityList[i].checked = false;
      }else{
        this.activityList[i].checked = true;
        this.choosen_activity = activity;
      }
   }
  }

  createActivity(){
    
    if(this.choosen_activity == null){
      console.log("activity type empty");
    }else{
      this.ui.loaderSetContent("Creazione attività...");
      this.ui.loaderPresent();
      this.activirysrv.create(this.local, this.schedule, this.choosen_activity, this.user).then(response =>{
        this.navCtrl.push(ShowActivityPage,{
          activityData: response
        });
        this.usrsrv.getUser(this.user).then(response =>{
          console.log("User logged");
          console.log(response);
          this.usrsrv.getUserActivity(response).then(response=>{
            
          }).catch(err=>{
            this.ui.loaderDismiss();
            console.log(err);
          })
        }).catch(err =>{
          console.log(err);
          this.ui.loaderDismiss();
        })
      }).catch(err =>{
        console.log(err);
        this.ui.loaderDismiss();
      })
    }
  }

}
