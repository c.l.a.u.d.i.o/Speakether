import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { AddLocalPage } from '../pages/add-local/add-local';
import { AddActivityPage } from '../pages/add-activity/add-activity';
import { SelectActivityPage } from "../pages/select-activity/select-activity";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Firebaseinit } from "../providers/providers";
import { Local } from "../providers/providers";
import { ActivityService } from "../providers/providers";
import { UserService } from "../providers/providers";
import { Uiproviders } from "../providers/providers";
import { UserPage } from '../pages/user/user';
import { ShowActivityPage } from '../pages/show-activity/show-activity';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    AddLocalPage,
    AddActivityPage,
    SelectActivityPage,
    ShowActivityPage,
    UserPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    AddLocalPage,
    AddActivityPage,
    SelectActivityPage,
    ShowActivityPage,
    UserPage
  ],
  providers: [
    Firebaseinit,
    Local,
    ActivityService,
    StatusBar,
    UserService,
    SplashScreen,
    Uiproviders,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
